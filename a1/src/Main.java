import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scannerName.nextLine();

        System.out.println("Last Name:");
        String lastName = scannerName.nextLine();

        System.out.println("First Subject Grade:");
        Double firstGrade = scannerName.nextDouble();

        System.out.println("Second Subject Grade:");
        Double secondGrade = scannerName.nextDouble();

        System.out.println("Third Subject Grade:");
        Double thirdGrade = scannerName.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName);
        Double aveGrade = (firstGrade + secondGrade + thirdGrade) / 3;
        System.out.println("Your grade average is: " + aveGrade);
    }
}