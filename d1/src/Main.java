import java.util.Scanner;

public class Main {
    /*
        Main Class

        The Main class is the entry point for our Java Program. it is responsible for executing our code.

        The main class usually has 1 method inside it, the main() method.
        The main method is the method to run our code.

        ctrl + / - single comments
        ctrl + shift + / - multi-line comments
    */

    public static void main(String[] args){
        /*
            Main Method is where most executable code is applied to.

            "public" is a access modifier which simply tells the application which classes have access to our method/attributes

            "static" means that the method/property belongs to the class. This means that it is accessible without having to create an instance of a class

            "void" means that this method will not return data, because in Java we have to declare the data type of the methods returned. And since, main does not return data, we add "void"
        */
        // System.out.println() is a statement which allows us to print the value of the argument passed into it in our terminal
        System.out.println("Kang Seulgi");
        System.out.println("Seulo soon");

        //In Java, to be able to declare a variable, we have to identify or declare its data type. Which means, that the variable will expect and only accept data with the type declared

        int myNum;
        myNum = 3;
        System.out.println(myNum);

        //myNum = "Sample String";

        myNum = 2500;
        System.out.println(myNum);

        int nationalPopulation = 2147483647;
        System.out.println(nationalPopulation);

        // L is added at the end of a long number to be recognized as long. Otherwise, it will be wrongfully recognized as int.
        long worldPopulation = 7862881145L;

        // we have to add f at the end of a float to be recognized as float
        float piDouble = 3.141592f;
        System.out.println(piDouble);

        //char - can only hold 1 character, using only single quote ('')
        char letter = 'a';
        System.out.println(letter);

        //string - can hold more than one character, using only double quotes ("")
        // letter = "b"

        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        //constants in Java is declared with final keyword and data type
        //final dataType VARIABLENAME
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        // final keyword can't be updated

        //String - in Java, it is a non-primitive data type. This is because Strings are actually objects that can use methods.
        //Non-primitive data types have access to methods:
        //array, class, interface

        String username = "yusukeUrameshi91";
        System.out.println(username);

        // String username = 'a';

        // Strings being non-primitive have access to methods
        //.isEmpty() is a string method which returns a boolean which checks the length of the string, and returns true if length is 0, otherwise false.
        System.out.println(username.isEmpty());

        String username2 = "";
        System.out.println(username2.isEmpty());

        //Scanner - is a class in Java which allows us to create an instance that accepts input from the terminal. It's like prompt() in JS.
        //However, Scanner being a class pre-defined by Java, has to be imported to be used

        Scanner scannerName = new Scanner(System.in);

        //.nextLine() receives user input and returns a string
        System.out.println("What is your name?");
        String myName = scannerName.nextLine();

        System.out.println("Your name is " + myName + "!");

        //.nextInt() receives user input and return an integer type
        System.out.println("What is your favorite number?");
        int myFaveNum = scannerName.nextInt();
        System.out.println(myFaveNum);

        //.nextDouble() receives use input and returns a double type:
        System.out.println("What was your average grade in highschool");
        double aveGrade = scannerName.nextDouble();
        System.out.println(aveGrade);

        //Much like in JS, Java also has similar mathematical operations
        System.out.println("Enter the first number");
        int number1 = scannerName.nextInt();

        System.out.println("Enter the second number");
        int number2 = scannerName.nextInt();

        int sum = number1 + number2;
        System.out.println("The sum of both numbers are: " + sum);

        System.out.println("Enter the first number");
        int number3 = scannerName.nextInt();

        System.out.println("Enter the second number");
        int number4 = scannerName.nextInt();

        int diff = number3 - number4;
        System.out.println("The difference of the numbers are: " + diff);
    }
}